import java.io.*;
import java.util.*;

public class Main {

    static ArrayList<File> files = new ArrayList<File>();


    public static void main(String[] args) {

        File dir = new File(inputDirName());
        String word = inputWord();
        TreeMap<String, String> charsets = takeCharsets();
        String[] numberCharsets = inputCharsets(charsets);

        if(dir.isDirectory()){
            walkthrough(dir);
        } else files.add(dir);


//        ArrayList<File> files = turnDirToList(dir);

        for (String number : numberCharsets) {
            System.out.println();
            System.out.println("***************************************************");
            System.out.println("Search using "+charsets.get(number)+" charset:");
            for (File file : files) {
                BufferedReader reader = getStream(file, charsets.get(number));
                ArrayList<String> stringList = getStringList(reader);
                printFound(findWord(stringList, word), file);
            }
        }


        //make a string
        //TODO search in words from 2 strings (переносы)
        //search in string
        //if is sout number of the string
    }

    //add charsets
    public static TreeMap<String, String> takeCharsets(){
        TreeMap<String, String> charsets = new TreeMap<String, String>();
        charsets.put("1", "UTF8");
        charsets.put("2", "Cp1251");
        charsets.put("3", "cp866");
        charsets.put("4", "KOI8_R");
        charsets.put("5", "UTF-16");
        charsets.put("6", "ASCII");
        return charsets;
    }

    //input filename or directory
    public static String inputDirName(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Input name of the file or directory:");
        while (true) {
            String dirName = scan.nextLine();
            if (new File(dirName).exists()){
                return dirName;
            } else {
                System.out.println("Wrong file or directory name. Input again:");
            }
        }
    }

    //turn directory to list
    public static void walkthrough(File dir){
        File[] entries = dir.listFiles();
        for (File subdir : entries){
            if (subdir.isDirectory()){
                walkthrough(subdir);
            } else {
                files.add(subdir);
            }
        }
    }

    //input the word
    public static String inputWord(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Input word to find:");
        return scan.nextLine().toLowerCase();
    }

    //choose charsets
    public static String[] inputCharsets(TreeMap<String, String> charsets){
        Scanner scan = new Scanner(System.in);
        while (true) {
            System.out.println("Choose charsets, you want to find in, separated by comma:");
            for (String num : charsets.keySet()){
                System.out.println(num+". "+charsets.get(num));
            }
            String charsetsStr = scan.nextLine();
            if (charsetsStr.matches("[[1-6][;,\\s]+]+")){
                return charsetsStr.split("[;,\\s]+");
            }
            System.out.println("Wrong spelling of charsets");
        }
    }

    //get stream
    public static BufferedReader getStream (File inputFile, String charset){
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile), charset));
        }   catch (FileNotFoundException e) {
            System.out.println("File or directory not found");
        } catch (UnsupportedEncodingException e){
            System.out.println("Wrong charset");
        }
        return rd;
    }

    //make arraylist of strings
    public static ArrayList<String> getStringList(BufferedReader rd){
        ArrayList<String> stringList = new ArrayList<String>();
        while (true) {
            try {
                String st;
                if ((st = rd.readLine()) != null){
                    stringList.add(st);
                } else return stringList;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //find a word in the arraylist of strings
    public static TreeMap<Integer, String> findWord (ArrayList<String> stringList, String word){
        TreeMap<Integer, String> found = new TreeMap<Integer, String>();
        int i = 0;
        for (String str : stringList){
            i++;
            if (str.toLowerCase().contains(word)){
                found.put(i, str);
            }
        }
        return found;
    }

    //print found strings
    public static void printFound(TreeMap<Integer, String> foundStrings, File file){
        if (!foundStrings.isEmpty()) {
            System.out.println();
            System.out.println("------------------------------------------------");
            System.out.println("File: " + file.getAbsolutePath());
            System.out.println();
            System.out.println("Here is found strings:");
            for (int number : foundStrings.keySet()){
                System.out.println("String number " + number+":");
                System.out.println(foundStrings.get(number));
            }
        }
    }
}